mod constants;
mod state;
mod action;
mod tree_gen;

use constants::Character;
use state::{State, SpeedState};
use action::{ActionKind, Action};

use tree_gen::{GraphGenerationData, build_option_tree};
use petgraph::{graph::Graph, stable_graph::NodeIndex, algo};
use std::ops::Index;

#[allow(unused)]
use strum::IntoEnumIterator;

#[allow(unused)]
use crate::constants::TargetSpeeds; 


fn main() {
    println!("Hello, world!");
    
    solve_for(GraphGenerationData {
        char: Character::Raptor,
        starting_action: ActionKind::Smash,
        end_action: ActionKind::End,
        excluded_actions: vec![ ActionKind::BadHalfCharge, ActionKind::HalfCharge ],
        min_speed: 8.0,
        starting_speed: 8.0,
        target_speed: 121.0,
        hitstun_limit: 50,
        depth: 10
    });
    
    /*
    for t_speed in TargetSpeeds::iter() {
        match t_speed {
            TargetSpeeds::HP50_Kill_OneHit |
            TargetSpeeds::HP200_Kill |
            TargetSpeeds::HP200_Kill_OneHit => continue,
            _ => {}
        }
        solve_for(GraphGenerationData {
            char: Character::Sonata,
            starting_action: ActionKind::Start,
            end_action: ActionKind::End,
            excluded_actions: vec![ ActionKind::HalfCharge, ActionKind::BadHalfCharge ],
            starting_speed: 8.0,
            target_speed: t_speed as i64 as f32,
            depth: 10
        });
    }
    */
    /*
    for char in Character::iter() {

        solve_for(GraphGenerationData {
            char: Character::Doombox,
            starting_action: ActionKind::Start,
            end_action: ActionKind::End,
            excluded_actions: vec![  ],
            starting_speed: 8.0,
            target_speed: 120.0,
            depth: 10
        });

    };
    */
}

fn solve_for(graph_gen: GraphGenerationData) {
    let mut graph =  Graph::<State, i64>::new();
    let start_end = build_option_tree( &mut graph, &graph_gen);
    print!("`{:12} | ", graph_gen.char);

    let path: Option<(i64, Vec<NodeIndex>)> = algo::astar(
        &graph, 
        start_end.0, 
        |node| node == start_end.1, 
        |edge| *edge.weight(),
        |_| 0);

    print_out_path(&graph, graph_gen, path);
    println!("`");
}

fn print_out_path (graph:&Graph<State, i64>, graph_gen: GraphGenerationData, path: Option<(i64, Vec<NodeIndex>)>) {
    match path {
        Some(path) => {
            print! ("{}: ", path.0);
            let nodes = path.1;
            for i in 0..nodes.len() {
                let state: &State = graph.index(nodes[i]);

                if i == 1 && state.action.kind == ActionKind::Start {
                    continue;
                }

                print! ("{}", state.action._get_action_name());
                if i < nodes.len() - 1 {
                    print! (" {:.2}", state.speed_state.speed);
                    //print! (", e:{}", state.energy);
                    print! (" -> ",);
                }
            }
        },
        None => print! (
            "No solution found to reach {:.0} speed in {:.0} hits with {:?} ",
            graph_gen.target_speed,
            graph_gen.depth - 1,
            graph_gen.char)
    }
}

