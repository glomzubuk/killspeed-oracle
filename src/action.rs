use crate::constants::*;
use crate::state::SpeedState;
use strum_macros::EnumIter;


#[derive(Debug)]
#[derive(EnumIter, Clone, Copy, PartialEq)]
pub enum ActionKind {
    Start,
    End,
    Bunt,
    Swing,
    BadHalfCharge,
    HalfCharge,
    Charge,
    Spike,
    Smash,
    LatchSmashBooS,
    LatchSpikeBooS,
    DoomboxSmashBooS,
    DoomboxSpikeBooS,
    NitroSmashBooS,
    NitroSpikeBooS,
    NitroChargeBooS,
    DiceSmashBooS,
    DiceSpikeBooS,
    DiceChargeBooS,
    CandySmash,
    CandySpike,
    SonataSpikeRecoveryCancel,
    SonataSmashRecoveryCancel,
    SonataSpeedGlitch,
    RaptorSmashQuickBooS,
    RaptorSmashLongSpin,
    DnASmashBooS,
    DnASpikeBooS,
    JetSmashBubble,
    JetSpikeBubble,
    JetSpeedGlitch,
    GridSmashBooS,
    GridChargeBooS,
    GridSpikeBooS,
    GridSmashSpikeport,
    GridChargeSpikeport,
}

fn hitstun_calculation(speed: f32, reduced: bool) -> i64 {
    const _FDELTA_TIME: f32 = 1.0 /60.0;
    const MAX_HITSTUN_DURATION: f32 = 120.0;
    let mut hitstun_duration:f32 = speed / 1.5;
    hitstun_duration = f32::min(hitstun_duration, MAX_HITSTUN_DURATION);
    if reduced {
        hitstun_duration /= 4.0;
    }
    hitstun_duration -= 1.25;

    //hitstun_duration *= FDELTA_TIME;
    return f32::ceil(hitstun_duration) as i64;
}

fn double_speed_calculation (speed_state: &SpeedState) -> f32 {
    speed_state.speed * f32::max(1.0 + f32::powi(0.75, speed_state.stack as i32), 1.1)
}

fn get_max_charge_time(char: Character) -> i64 {
    let char_specific_charge_time = match char {
        Character::Latch => 15,
        Character::Toxic |
        Character::Raptor |
        Character::DustAndAshes => 16,
        Character::Dice |
        Character::Candyman|
        Character::Nitro => 17,
        Character::Sonata => 18,
        Character::Doombox => 19,
        Character::Grid |
        Character::Switch => 20,
        Character::Jet => 21,
    };
    return  13 + char_specific_charge_time
}

#[derive(Debug,)]
#[derive(Clone, Copy)]
pub struct Action {
    pub kind: ActionKind,
}

// TODO Add hitlag dependent delay preventing special startup on certain characters
impl Action {
    pub fn get_time_data(&self, char: Character, skip_startup: bool, speed_glitch: bool) -> i64 {
        match self.kind {
            ActionKind::Start |
            ActionKind::End => 0,
            ActionKind::SonataSmashRecoveryCancel |
            ActionKind::SonataSpikeRecoveryCancel => HITSTUN_WALL,
            ActionKind::Bunt => RECOVERY_BUNT,
            ActionKind::Swing => RECOVERY_SWING,
            ActionKind::BadHalfCharge |
            ActionKind::HalfCharge |
            ActionKind::Charge => {
                if skip_startup {
                    RECOVERY_SWING
                } else if speed_glitch {
                    18 + RECOVERY_SWING
                } else {
                    get_max_charge_time(char) - match self.kind {
                        ActionKind::BadHalfCharge =>  7,
                        ActionKind::HalfCharge =>  4,
                        ActionKind::Charge => 3,
                        _ => 0
                    } + RECOVERY_SWING
                }
            },
            ActionKind::SonataSpeedGlitch => {
                get_max_charge_time(char) - 3 + 30
            }
            ActionKind::JetSpeedGlitch => {
                get_max_charge_time(char) - 3 + 46 + 7
            }
            ActionKind::CandySpike |
            ActionKind::JetSpikeBubble |
            ActionKind::Spike => match char {
                Character::Candyman => 11,
                _ => 12
            }
            ActionKind::CandySmash |
            ActionKind::JetSmashBubble |
            ActionKind::Smash => match char {
                Character::Doombox => 10,
                _ => 11
            }
            ActionKind::GridChargeBooS |
            ActionKind::GridSmashBooS |
            ActionKind::GridSpikeBooS => 1 + 9 + 8 + 9 + 1 + RECOVERY_BUNT,
            ActionKind::GridChargeSpikeport |
            ActionKind::GridSmashSpikeport => 1 + 9 + 8 + 20 + 1 + RECOVERY_SPIKE,
            ActionKind::DiceChargeBooS => SPECIAL_DICE_STARTUP + 4 + 10,
            ActionKind::DiceSmashBooS => SPECIAL_DICE_STARTUP + 4 + 10,
            ActionKind::DiceSpikeBooS => SPECIAL_DICE_STARTUP + 2 + 11,
            ActionKind::RaptorSmashLongSpin => 1 + 15 + 9 + RECOVERY_BUNT,
            ActionKind::RaptorSmashQuickBooS => 1 + 15 + 16 + 14 + 15,
            ActionKind::LatchSmashBooS |
            ActionKind::LatchSpikeBooS => 13 + 7 + 14,
            ActionKind::DoomboxSmashBooS |
            ActionKind::DoomboxSpikeBooS => 18 + 9 + 12,
            ActionKind::DnASmashBooS |
            ActionKind::DnASpikeBooS => 1 + 21 + 0 + 1 + 16 + 17, // hitlag dependent delay + eat the ball + go to the ground + ground travel + bunt out + returning to dust 
            ActionKind::NitroSmashBooS |
            ActionKind::NitroSpikeBooS |
            ActionKind::NitroChargeBooS => 2 + 4 + RECOVERY_BUNT,
        }
    }

    pub fn reduces_next(&self) -> bool {
        match self.kind {
            ActionKind::LatchSmashBooS |
            ActionKind::LatchSpikeBooS |
            ActionKind::DoomboxSmashBooS |
            ActionKind::DoomboxSpikeBooS |
            ActionKind::NitroChargeBooS |
            ActionKind::NitroSpikeBooS |
            ActionKind::NitroSmashBooS |
            ActionKind::DiceChargeBooS |
            ActionKind::DiceSpikeBooS |
            ActionKind::DiceSmashBooS |
            ActionKind::RaptorSmashQuickBooS |
            ActionKind::CandySmash |
            ActionKind::CandySpike |
            ActionKind::DnASmashBooS |
            ActionKind::DnASpikeBooS |
            ActionKind::JetSmashBubble |
            ActionKind::JetSpikeBubble |
            ActionKind::JetSpeedGlitch |
            ActionKind::GridChargeBooS |
            ActionKind::GridSmashBooS |
            ActionKind::GridSpikeBooS |
            ActionKind::Bunt => true,
            ActionKind::Start |
            ActionKind::End |
            ActionKind::Swing |
            ActionKind::BadHalfCharge |
            ActionKind::HalfCharge |
            ActionKind::SonataSpeedGlitch |
            ActionKind::SonataSmashRecoveryCancel |
            ActionKind::SonataSpikeRecoveryCancel |
            ActionKind::RaptorSmashLongSpin |
            ActionKind::GridChargeSpikeport |
            ActionKind::GridSmashSpikeport |
            ActionKind::Spike |
            ActionKind::Smash |
            ActionKind::Charge => {
                false
            },
        }
    }
    pub fn get_hitstun (&self, speed: f32, reduced: bool) -> i64 {
        match self.kind {
            ActionKind::Start |
            ActionKind::End => 0,
            ActionKind::DoomboxSmashBooS |
            ActionKind::DoomboxSpikeBooS |
            ActionKind::DiceChargeBooS |
            ActionKind::DiceSpikeBooS |
            ActionKind::DiceSmashBooS |
            ActionKind::RaptorSmashLongSpin |
            ActionKind::RaptorSmashQuickBooS |
            ActionKind::DnASmashBooS |
            ActionKind::DnASpikeBooS |
            ActionKind::LatchSmashBooS |
            ActionKind::LatchSpikeBooS => 0,
            ActionKind::Bunt => HITSTOP_BUNT,
            ActionKind::Swing |
            ActionKind::BadHalfCharge |
            ActionKind::HalfCharge |
            ActionKind::CandySpike |
            ActionKind::Spike |
            ActionKind::CandySmash |
            ActionKind::SonataSmashRecoveryCancel |
            ActionKind::SonataSpikeRecoveryCancel |
            ActionKind::NitroSmashBooS |
            ActionKind::NitroSpikeBooS |
            ActionKind::JetSmashBubble |
            ActionKind::JetSpikeBubble |
            ActionKind::GridSmashBooS |
            ActionKind::GridSmashSpikeport |
            ActionKind::GridSpikeBooS |
            ActionKind::Smash => {
                hitstun_calculation(speed, reduced)
            },
            ActionKind::JetSpeedGlitch |
            ActionKind::SonataSpeedGlitch |
            ActionKind::GridChargeBooS |
            ActionKind::GridChargeSpikeport |
            ActionKind::NitroChargeBooS |
            ActionKind::Charge => {
                hitstun_calculation(speed, true)
            },
        }
    }
    pub fn update_speed(&self, speed_state: &SpeedState, min_speed: f32, char: Character) -> SpeedState {
        let mut ss_result = SpeedState::from(*speed_state);
        match self.kind {
            ActionKind::Start |
            ActionKind::End => {},
            ActionKind::Bunt => {
                ss_result.speed -= 3.0;
            },
            ActionKind::Swing => ss_result.speed += 1.0,
            ActionKind::BadHalfCharge => ss_result.speed += 7.0,
            ActionKind::HalfCharge => {
                ss_result.speed += match char {
                    Character::Switch => 9.0,
                    _ => 8.0
                }
            },
            ActionKind::LatchSpikeBooS |
            ActionKind::DiceSpikeBooS |
            ActionKind::CandySpike |
            ActionKind::SonataSpikeRecoveryCancel |
            ActionKind::JetSpikeBubble |
            ActionKind::Spike => {
                ss_result.speed += 1.0;
                ss_result.stack = 0;
            },
            ActionKind::LatchSmashBooS |
            ActionKind::DiceChargeBooS |
            ActionKind::DiceSmashBooS |
            ActionKind::SonataSmashRecoveryCancel |
            ActionKind::JetSmashBubble |
            ActionKind::CandySmash |
            ActionKind::Charge |
            ActionKind::Smash => {
                ss_result.speed = double_speed_calculation(&ss_result);
                ss_result.stack += 1;
            },
            ActionKind::DoomboxSpikeBooS |
            ActionKind::GridSpikeBooS |
            ActionKind::DnASpikeBooS |
            ActionKind::NitroSpikeBooS => {
                ss_result.speed += 1.0;
                ss_result.stack = 0;
                ss_result.speed -= 3.0;

            }
            ActionKind::DoomboxSmashBooS |
            ActionKind::RaptorSmashQuickBooS |
            ActionKind::DnASmashBooS |
            ActionKind::GridChargeBooS |
            ActionKind::GridSmashBooS |
            ActionKind::NitroSmashBooS |
            ActionKind::NitroChargeBooS => {
                ss_result.speed = double_speed_calculation(&ss_result);
                ss_result.stack += 1;
                ss_result.speed -= 3.0;
            },
            ActionKind::GridChargeSpikeport |
            ActionKind::GridSmashSpikeport => {
                ss_result.speed = double_speed_calculation(&ss_result);
                ss_result.stack = 0;
            }
            ActionKind::JetSpeedGlitch |
            ActionKind::RaptorSmashLongSpin => {
                ss_result.speed = double_speed_calculation(&ss_result);
                ss_result.stack += 1;
                ss_result.speed = double_speed_calculation(&ss_result);
                ss_result.stack += 1;
            },
            ActionKind::SonataSpeedGlitch => {
                ss_result.speed = double_speed_calculation(&ss_result);
                ss_result.stack += 1;

                ss_result.speed += 1.0;
                
                ss_result.speed = double_speed_calculation(&ss_result);
                ss_result.stack += 1;
            },
        }
        ss_result.speed = f32::max(ss_result.speed, min_speed);
        return ss_result;
    }
    pub fn update_energy(&self, energy: u8, bunted: bool) -> u8 {
        let e: u8 = match self.kind {
            ActionKind::Start |
            ActionKind::DoomboxSmashBooS |
            ActionKind::DoomboxSpikeBooS |
            ActionKind::SonataSpeedGlitch |
            ActionKind::SonataSmashRecoveryCancel |
            ActionKind::SonataSpikeRecoveryCancel |
            ActionKind::JetSpeedGlitch |
            ActionKind::JetSmashBubble |
            ActionKind::JetSpikeBubble |
            ActionKind::NitroChargeBooS |
            ActionKind::NitroSmashBooS |
            ActionKind::NitroSpikeBooS |
            ActionKind::DiceChargeBooS |
            ActionKind::DiceSmashBooS |
            ActionKind::DiceSpikeBooS |
            ActionKind::RaptorSmashLongSpin |
            ActionKind::RaptorSmashQuickBooS |
            ActionKind::DnASmashBooS |
            ActionKind::DnASpikeBooS |
            ActionKind::GridChargeBooS |
            ActionKind::GridSmashBooS |
            ActionKind::GridSpikeBooS |
            ActionKind::GridChargeSpikeport |
            ActionKind::GridSmashSpikeport |
            ActionKind::LatchSmashBooS |
            ActionKind::LatchSpikeBooS => 0,
            ActionKind::End |
            ActionKind::Bunt => energy,
            ActionKind::Spike => energy + 1,
            ActionKind::Swing |
            ActionKind::BadHalfCharge |
            ActionKind::HalfCharge |
            ActionKind::Smash |
            ActionKind::Charge => {
                if bunted {
                    energy + 2
                } else {
                    energy + 1
                }
            },
            ActionKind::CandySpike |
            ActionKind::CandySmash => {
                if energy > 1 {
                    0
                } else {
                    1
                }
            },
        };
        return u8::min(e, 4)
    }
    pub fn update_bubble(&self, bubble_count: u8) -> u8 {
        match self.kind {
            ActionKind::JetSpeedGlitch |
            ActionKind::JetSmashBubble |
            ActionKind::JetSpikeBubble => bubble_count + 1,
            _ => 0
        }
    }

    pub fn is_bunt(&self) -> bool {
        match self.kind {
            ActionKind::Bunt |

            ActionKind::LatchSmashBooS |
            ActionKind::LatchSpikeBooS |
            ActionKind::DoomboxSmashBooS |
            ActionKind::DoomboxSpikeBooS |
            ActionKind::NitroSmashBooS |
            ActionKind::NitroSpikeBooS |
            ActionKind::NitroChargeBooS |
            ActionKind::DiceSmashBooS |
            ActionKind::DiceSpikeBooS |
            ActionKind::DiceChargeBooS |
            ActionKind::RaptorSmashQuickBooS |
            ActionKind::DnASmashBooS |
            ActionKind::DnASpikeBooS |
            ActionKind::GridSmashBooS |
            ActionKind::GridChargeBooS |
            ActionKind::GridSpikeBooS => true,
            _ => false,
        }
    }

    pub fn _get_action_name(&self) -> &str {
        match self.kind {
            ActionKind::Start => "Start",
            ActionKind::End => "End",
            ActionKind::Bunt => "Bunt",
            ActionKind::Swing => "Swing",
            ActionKind::BadHalfCharge => "BadHalfCharge",
            ActionKind::HalfCharge => "HalfCharge",
            ActionKind::Charge => "Charge",
            ActionKind::Spike => "Spike",
            ActionKind::Smash => "Smash",
            ActionKind::LatchSmashBooS => "LatchSmashBooS",
            ActionKind::LatchSpikeBooS => "LatchSpikeBooS",
            ActionKind::DoomboxSmashBooS => "DoomboxSmashBooS",
            ActionKind::DoomboxSpikeBooS => "DoomboxSpikeBooS",
            ActionKind::CandySmash => "CandySmash",
            ActionKind::CandySpike => "CandySpike",
            ActionKind::SonataSpeedGlitch => "SonataSpeedGlitch",
            ActionKind::SonataSmashRecoveryCancel => "SonataSmashRecoveryCancel",
            ActionKind::SonataSpikeRecoveryCancel => "SonataSpikeRecoveryCancel",
            ActionKind::NitroChargeBooS => "NitroChargeBooS ",
            ActionKind::NitroSmashBooS => "NitroSmashBooS",
            ActionKind::NitroSpikeBooS => "NitroSpikeBooS",
            ActionKind::DiceChargeBooS => "DiceChargeBooS",
            ActionKind::DiceSmashBooS => "DiceSmashBooS",
            ActionKind::DiceSpikeBooS => "DiceSpikeBooS",
            ActionKind::RaptorSmashLongSpin => "RaptorSmashLongSpin",
            ActionKind::RaptorSmashQuickBooS => "RaptorSmashQuickBooS",
            ActionKind::DnASmashBooS => "DnASmashBooS",
            ActionKind::DnASpikeBooS => "DnASpikeBooS",
            ActionKind::JetSpeedGlitch => "JetSpeedGlitch",
            ActionKind::JetSmashBubble => "JetSmashBubble",
            ActionKind::JetSpikeBubble => "JetSpikeBubble",
            ActionKind::GridChargeBooS => "GridChargeBooS",
            ActionKind::GridSmashBooS => "GridSmashBooS",
            ActionKind::GridSpikeBooS => "GridSpikeBooS",
            ActionKind::GridChargeSpikeport => "GridChargeSpikeport",
            ActionKind::GridSmashSpikeport => "GridSmashSpikeport",
        }
    }
}