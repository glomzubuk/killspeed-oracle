use crate::{Action, ActionKind, Character, State, SpeedState};

use petgraph::{graph::Graph, stable_graph::NodeIndex};
use std::ops::Index;
use strum::IntoEnumIterator; 


#[derive(Clone)]
pub struct GraphGenerationData {
    pub char: Character,
    pub starting_action: ActionKind,
    pub end_action: ActionKind,
    pub excluded_actions: Vec<ActionKind>,
    pub min_speed: f32,
    pub starting_speed: f32,
    pub target_speed: f32,
    pub hitstun_limit: i64,
    pub depth: i32,
}
pub fn build_option_tree(graph: &mut Graph<State, i64>, graph_gen: &GraphGenerationData) -> (NodeIndex, NodeIndex) {
    let start_state: State = State {
        speed_state: SpeedState { speed: graph_gen.starting_speed, stack: 0 },
        energy: 0,
        bubble_count: 0,
        bunted: false,
        action: Action { kind: ActionKind::Start },
    };
    let end_state: State = State {
        speed_state: SpeedState { speed: 0.0, stack: 0 },
        energy: 0,
        bubble_count: 0,
        bunted: false,
        action: Action { kind: ActionKind::End },
    }; 
    let start_node_index = graph.add_node(start_state);
    let end_node_index = graph.add_node(end_state);

    let depth = if graph_gen.starting_action == ActionKind::Start {
        graph_gen.depth + 1
    } else {
        graph_gen.depth
    };

    tree_descend(graph, start_node_index, end_node_index, graph_gen.starting_action, graph_gen, depth);
    return (start_node_index, end_node_index);
}

fn can_do_action(char: Character, action_kind: ActionKind) -> bool {
    match action_kind {
        ActionKind::LatchSmashBooS |
        ActionKind::LatchSpikeBooS => {
            if char != Character::Latch {
                return false;
            }
        },
        ActionKind::CandySmash |
        ActionKind::CandySpike => {
            if char != Character::Candyman {
                return false;
            }
        }
        ActionKind::DoomboxSmashBooS |
        ActionKind::DoomboxSpikeBooS => {
            if char != Character::Doombox {
                return false;
            }
        },
        ActionKind::RaptorSmashLongSpin |
        ActionKind::RaptorSmashQuickBooS => {
            if char != Character::Raptor {
                return false;
            }
        },
        ActionKind::SonataSmashRecoveryCancel |
        ActionKind::SonataSpikeRecoveryCancel |
        ActionKind::SonataSpeedGlitch => {
            if char != Character::Sonata {
                return false;
            }
        },
        ActionKind::NitroChargeBooS |
        ActionKind::NitroSmashBooS |
        ActionKind::NitroSpikeBooS => {
            if char != Character::Nitro {
                return false;
            }
        },
        ActionKind::DiceChargeBooS |
        ActionKind::DiceSmashBooS |
        ActionKind::DiceSpikeBooS => {
            if char != Character::Dice {
                return false;
            }
        },
        ActionKind::DnASmashBooS |
        ActionKind::DnASpikeBooS => {
            if char != Character::DustAndAshes {
                return false;
            }
        },
        ActionKind::JetSpeedGlitch |
        ActionKind::JetSmashBubble |
        ActionKind::JetSpikeBubble => {
            if char != Character::Jet {
                return false;
            }
        },
        ActionKind::GridChargeBooS |
        ActionKind::GridSmashBooS |
        ActionKind::GridSpikeBooS |
        ActionKind::GridChargeSpikeport |
        ActionKind::GridSmashSpikeport =>  {
            if char != Character::Grid {
                return false;
            }
        },
        
        _ => { return true; }
    };
    return true;
}


fn tree_descend(graph: &mut Graph<State, i64>, parent_index: NodeIndex, end_index:NodeIndex, action_kind: ActionKind, graph_gen: &GraphGenerationData, depth: i32) {
    if graph_gen.excluded_actions.contains(&action_kind) {
        return;
    }

    if !can_do_action(graph_gen.char, action_kind) {
        return;
    }

    let parent_node: &State = graph.index(parent_index);
    let parent_action = parent_node.action;
    let reduced = parent_action.reduces_next();
    let action = Action { kind: action_kind };
    let energy = action.update_energy(parent_node.energy, parent_node.bunted);
    let bubble_count = action.update_bubble(parent_node.bubble_count);

    if bubble_count > 2 {
        return;
    }


    let state: State = State {
        speed_state: action.update_speed(&parent_node.speed_state, graph_gen.min_speed, graph_gen.char),
        energy,
        action,
        bubble_count,
        bunted: action.is_bunt()
    };
    let skip_startup = depth == graph_gen.depth;
    let speed_glitch = match parent_action.kind {
        ActionKind::SonataSpeedGlitch |
        ActionKind::JetSpeedGlitch => true,
        _ => false,
    };
    let mut action_hitstun = state.action.get_hitstun(state.speed_state.speed, reduced);
    let mut action_timedata =  state.action.get_time_data(graph_gen.char, skip_startup, speed_glitch);


    match action_kind {
        ActionKind::GridChargeBooS |
        ActionKind::GridSmashBooS |
        ActionKind::GridSpikeBooS |
        ActionKind::GridChargeSpikeport |
        ActionKind::GridSmashSpikeport => {
            const GRID_QUICKPORT_TIMESAVE:i64 = 9;
            if action_hitstun > GRID_QUICKPORT_TIMESAVE {
                action_hitstun = 0;
            } else {
                action_timedata -= GRID_QUICKPORT_TIMESAVE;
            }
        },
        _ => {}
    }

    if action_hitstun > graph_gen.hitstun_limit {
        return;
    }

    let node_time: i64 = action_hitstun + action_timedata;

    let node_index = graph.add_node(state);
    graph.add_edge(parent_index, node_index, node_time);

    if state.speed_state.speed > graph_gen.target_speed {
        if graph_gen.end_action != ActionKind::End && action_kind != graph_gen.end_action {
            return;
        }
        graph.add_edge(node_index, end_index, 0);
        return;
    } else if  depth - 1 <= 0 {
        return;
    }
    for next_action_kind in ActionKind::iter() {
        if match action_kind {
            ActionKind::CandySmash |
            ActionKind::CandySpike => true,
            _ => false
        } {
            match next_action_kind {
                ActionKind::CandySmash |
                ActionKind::CandySpike => {
                },
                _ => {
                    continue
                },
            }
        } else {
            match next_action_kind {
                ActionKind::Start |
                ActionKind::Swing |
                ActionKind::End => continue,
                ActionKind::HalfCharge |
                ActionKind::BadHalfCharge => {
                    if depth < graph_gen.depth - 1 {
                        continue
                    }
                },
                ActionKind::Bunt => {
                    if reduced {
                        continue;
                    }
                }
                ActionKind::CandySmash |
                ActionKind::DoomboxSmashBooS |
                ActionKind::SonataSpeedGlitch |
                ActionKind::SonataSmashRecoveryCancel |
                ActionKind::NitroChargeBooS |
                ActionKind::NitroSmashBooS |
                ActionKind::DiceChargeBooS |
                ActionKind::DiceSmashBooS |
                ActionKind::GridChargeBooS |
                ActionKind::GridSmashBooS |
                ActionKind::GridChargeSpikeport |
                ActionKind::GridSmashSpikeport |
                ActionKind::RaptorSmashLongSpin |
                ActionKind::RaptorSmashQuickBooS |
                ActionKind::DnASmashBooS |
                ActionKind::LatchSmashBooS => {
                    if (reduced && energy < 2) || (energy < 3) {
                        continue;
                    }
                },
                ActionKind::SonataSpikeRecoveryCancel |
                ActionKind::CandySpike |
                ActionKind::DoomboxSpikeBooS |
                ActionKind::NitroSpikeBooS |
                ActionKind::GridSpikeBooS |
                ActionKind::DiceSpikeBooS |
                ActionKind::DnASpikeBooS |
                ActionKind::LatchSpikeBooS => {
                    if energy < 3 {
                        continue;
                    }
                },
                ActionKind::JetSpeedGlitch |
                ActionKind::JetSpikeBubble |
                ActionKind::JetSmashBubble => {
                    if match action_kind {
                        ActionKind::JetSpeedGlitch |
                        ActionKind::JetSmashBubble |
                        ActionKind::JetSpikeBubble => false,
                        _ => true
                    } {
                        if next_action_kind == ActionKind::JetSpikeBubble {
                            if energy < 3 {
                                continue;
                            }
                        } else {
                            if (reduced && energy < 2) || (energy < 3) {
                                continue;
                            }
                        }
                    }
                }
                ActionKind::Charge |
                ActionKind::Smash |
                ActionKind::Spike => {},
            }
        }

        tree_descend(graph, node_index, end_index, next_action_kind, graph_gen, depth-1);
    }
}
