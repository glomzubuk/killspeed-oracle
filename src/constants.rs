use strum_macros::EnumIter;

#[allow(dead_code)]
#[derive(strum_macros::Display)]
#[derive(Clone, Copy, PartialEq, Debug, EnumIter)]
pub enum Character {
    Toxic, // x
    Grid, // x
    Dice, // x
    Latch, // x
    Sonata, // x
    Switch, // x
    Raptor, // x
    Candyman, // x
    Jet, // x
    Doombox, // x
    Nitro, // x
    DustAndAshes, // x
}

 #[allow(dead_code, non_camel_case_types)]
 #[derive(Clone, Copy, PartialEq, Debug, EnumIter)]
 pub enum TargetSpeeds {
    HP100_Kill = 120,
    HP100_Kill_OneHit = 120 - 120/5,
    HP50_Kill = 60,
    HP50_Kill_OneHit = 60 - 60/5,
    HP200_Kill = 240,
    HP200_Kill_OneHit = 240 - 240/5,
}

pub const HITSTOP_BUNT: i64 = 5;
pub const HITSTUN_WALL: i64 = 4;
pub const RECOVERY_SWING: i64 = 14;
pub const RECOVERY_SPIKE: i64 = 11;
pub const RECOVERY_BUNT: i64 = 11;
pub const SPECIAL_DICE_STARTUP: i64 = 23;