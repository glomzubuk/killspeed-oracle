use crate::Action;

#[derive(Debug)]
#[derive(Clone, Copy)]
pub struct State {
    pub action: Action,
    pub speed_state: SpeedState,
    pub energy: u8,
    pub bunted: bool,
    pub bubble_count: u8,
}

#[derive(Debug)]
#[derive(Clone, Copy)]
pub struct SpeedState {
    pub speed: f32,
    pub stack: u8,
}
